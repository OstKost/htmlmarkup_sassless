/*jshint esversion: 6 */

const DELAY = 500;
const ANIMATION_DELAY = 3000;
let currentSlide = 1;

const slider = document.querySelector(".slider");

slider.addEventListener("click", event => {
	if (!event.target.classList.contains("slider__icon")) return;
	currentSlide = parseInt(event.target.getAttribute("dataId"));
	showSliderID(currentSlide);
});

initSlider();

function showSliderID(id) {
	const current = slider.querySelector(`[dataSlide="${id}"]`);

	slider.querySelectorAll(".slider__slide").forEach(slide => {
		slide.style.opacity = 0;
		setTimeout(() => {
			slide.classList.add("hide");
		}, DELAY);
	});

	setTimeout(() => {
		current.classList.remove("hide");
		current.style.opacity = 1;
	}, DELAY);
}

function initSlider() {
	setInterval(() => {
		showSliderID(getSliderId());
	}, ANIMATION_DELAY);
}

function getSliderId() {
	currentSlide++;
	currentSlide = currentSlide % 4 === 0 ? 1 : currentSlide;
	return currentSlide;
}
